import random

from player_abalone import PlayerAbalone
from seahorse.game.action import Action
from seahorse.game.game_state import GameState


class MyPlayer(PlayerAbalone):
    """
    Player class for Abalone game that makes random moves.

    Attributes:
        piece_type (str): piece type of the player
    """

    def __init__(self, piece_type: str, name: str = "bob", time_limit: float=60*15,*args) -> None:
        """
        Initialize the PlayerAbalone instance.

        Args:
            piece_type (str): Type of the player's game piece
            name (str, optional): Name of the player (default is "bob")
            time_limit (float, optional): the time limit in (s)
        """
        super().__init__(piece_type,name,time_limit,*args)


    def compute_action(self, current_state: GameState, **kwargs) -> Action:
        """
        Function to implement the logic of the player (here random selection of a feasible solution).

        Args:
            current_state (GameState): Current game state representation
            **kwargs: Additional keyword arguments

        Returns:
            Action: Randomly selected feasible action
        """
        # ALEATOIRE ALEATOIRE  ALEATOIRE ALEATOIRE  ALEATOIRE ALEATOIRE  ALEATOIRE ALEATOIRE  ALEATOIRE ALEATOIRE 
        # possible_actions = current_state.get_possible_actions()
        # random.seed("seahorse")
        # if kwargs:
        #     pass
        # return random.choice(list(possible_actions))
        #  ALEATOIRE ALEATOIRE  ALEATOIRE ALEATOIRE  ALEATOIRE ALEATOIRE  ALEATOIRE ALEATOIRE  ALEATOIRE ALEATOIRE 
        def calculate_distance_to_center(position):
            position_map = {
                (0, 0): 0,
                (0, 8): 0,
                (1, 2): 0,
                (1, 3): 0,
                (1, 5): 0,
                (1, 6): 0,  
                (1, 1): 1,
                (1, 7): 1,
                (2, 2): 2,
                (2, 3): 2,
                (2, 4): 2,
                (2, 5): 2,
                (2, 1): 1,
                (2, 6): 1,
                (3, 3): 3,
                (3, 4): 3,
                (3, 2): 2,
                (3, 5): 2,
                (3, 1): 1,
                (3, 6): 1,
                (3, 0): 0,
                (3, 7): 0,
                (4, 4): 4,
                (4, 3): 3,
                (4, 5): 3,
                (4, 2): 2,
                (4, 6): 2,
                (4, 1): 1,
                (4, 7): 1,
                (4, 0): 0,
                (4, 8): 0,
            }

            return position_map.get(position, 0)
        
        possible_actions = current_state.get_possible_actions()
        action_number = 0
        max_distance = 0
        action_min = 0
        optimal_grid = []
        best_action = None
        for action in possible_actions:
            board = action.next_game_state.rep.get_grid()
            distance_action = 0
            for i in range(len(board)):
                for j in range(len(board[i])):
                    if board[i][j] == 'W':
                        position = (i, j)
                        distance = calculate_distance_to_center(position)
                        distance_action += distance
            if distance_action > max_distance:
                max_distance = distance_action
                action_min = action_number
                optimal_grid = board
                best_action = action
            action_number += 1
        
        return best_action
