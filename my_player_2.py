from player_abalone import PlayerAbalone
from seahorse.game.action import Action
from seahorse.game.game_state import GameState
from seahorse.utils.custom_exceptions import MethodNotImplementedError


class MyPlayer(PlayerAbalone):
    """
    Player class for Abalone game.

    Attributes:
        piece_type (str): piece type of the player
    """

    def __init__(self, piece_type: str, name: str = "bob", time_limit: float = 60 * 15, *args) -> None:
        """
        Initialize the PlayerAbalone instance.

        Args:
            piece_type (str): Type of the player's game piece
            name (str, optional): Name of the player (default is "bob")
            time_limit (float, optional): the time limit in (s)
        """
        self.config_type = None
        super().__init__(piece_type, name, time_limit, *args)

    def compute_action(self, current_state: GameState, **kwargs) -> Action:
        """
        Function to implement the logic of the player.

        Args:
            current_state (GameState): Current game state representation
            **kwargs: Additional keyword arguments

        Returns:
            Action: selected feasible action
        """
        #  Déterminer si on est en mode Classique ou Alien :

        mode_board = "Classique"

        ### ALGORITHME ALPHA - BETA PRUNING
        def alphabetaPurning(current_state, depth, alpha, beta, isMaximizingPlayer, previous_state, pre_previous_state, mode_board):
            if(current_state.get_step()< 9 and depth == 3):
                depth-=1
            if isMaximizingPlayer:
                if depth == 0 or current_state.is_done():
                    return advanced_heuristic(current_state, previous_state, pre_previous_state), None
                bestValue = -float('inf')

                for action in current_state.get_possible_actions():
                    value, _ = alphabetaPurning(action.next_game_state, depth - 1, alpha, beta, False, current_state, previous_state, mode_board)
                    if value > bestValue:
                        bestValue = value
                        best_action = action
                    alpha = max(alpha, bestValue)
                    if bestValue >= beta:
                        return bestValue, best_action  # Beta cut-off

            else:
                if depth == 0 or current_state.is_done():
                    return advanced_heuristic(current_state, previous_state, pre_previous_state), None
                bestValue = float('inf')
                for action in current_state.get_possible_actions():
                    value, _ = alphabetaPurning(action.next_game_state, depth - 1, alpha, beta, True, current_state, previous_state, mode_board)
                    if value < bestValue:
                        bestValue = value
                        best_action = action
                    beta = min(beta, bestValue)
                    if bestValue <= alpha:
                        return bestValue, best_action  # Alpha cut-off
            return bestValue, best_action

        ### LES FONCTIONS DE CALCULS
        def manhattanDist(A, B):
            mask1 = [(0, 2), (1, 3), (2, 4), (3, 5), (4, 6), (5, 7), (6, 8)]
            mask2 = [(0, 4)]
            diff = (abs(B[0] - A[0]), abs(B[1] - A[1]))
            dist = (abs(B[0] - A[0]) + abs(B[1] - A[1])) / 2
            if diff in mask1:
                dist += 1
            if diff in mask2:
                dist += 2
            return dist

        def calculate_distance_to_center(position, center):
            distance = manhattanDist(center, position)
            return distance

        def calculate_cohesion(piece_positions):

            total_cohesion = 0
            num_pairs = 0

            for i, position1 in enumerate(piece_positions):
                for j, position2 in enumerate(piece_positions):
                    if i < j:  # Avoid counting pairs twice
                        total_cohesion += manhattanDist(position1, position2)
                        num_pairs += 1

            if num_pairs == 0:
                return 0  # Avoid division by zero if there are no pairs

            average_cohesion = total_cohesion / num_pairs

            return average_cohesion

        def closeTo_opponent(my_pieces, opponent_pieces):
            # Calculate the total push score
            close_score = 0
            for my_position in my_pieces:
                for opponent_position in opponent_pieces:
                    # Adjust the weight based on your preferences
                    close_score += 1 / manhattanDist(my_position, opponent_position)

            return close_score

        def push_opponent(my_pieces, previous_opponent_pieces):
            push_score =0
            for position in my_pieces:
                for previous_opponent_piece in previous_opponent_pieces:
                    if position == previous_opponent_piece:
                        push_score+=10  #Eviter d'avoir un poids déraisonnablement grand

            return push_score

        def mobility(game_state, possible_actions):
            mobility_score = len(possible_actions)
            return mobility_score

        def is_three_pack(center_piece, board, piece_type):
            directions = [
                (1, 0), (-1, 0),  # vertical
                (0, 1), (0, -1),  # horizontal
                (1, -1), (-1, 1)  # diagonal
            ]

            for dir in directions:
                adjacent_count = 0
                for step in range(1, 3):
                    adj_row = center_piece[0] + dir[0] * step
                    adj_col = center_piece[1] + dir[1] * step

                    if 0 <= adj_row < len(board) and 0 <= adj_col < len(board[0]):
                        if board[adj_row][adj_col] == piece_type:
                            adjacent_count += 1
                        else:
                            break  # Stop checking this direction if a non-matching piece is found
                    else:
                        break  # Out of bounds

                if adjacent_count == 2:
                    return True  # Found a pack of 3

            return False

        def count_three_packs(board, piece_type):
            three_pack_count = 0
            for i in range(len(board)):
                for j in range(len(board[i])):
                    if board[i][j] == piece_type:
                        if is_three_pack((i, j), board, piece_type):
                            three_pack_count += 1
            return three_pack_count // 3

        def auBord(my_pieces):
            auBord_score = 0
            for position in my_pieces:
                if (calculate_distance_to_center(position,center=(4,4))>=3):
                    auBord_score+=1
            return auBord_score

        def determine_mode(my_piece_count, opponent_piece_count, current_state):
            piece_diff = my_piece_count - opponent_piece_count
            mode = "neutral"
            if (0 <= current_state.get_step() < 8):
                mode = "neutral"
            elif (8<= current_state.get_step()):
                mode = "aggressive"
            if piece_diff >= 1:
                return "defensive"
            elif piece_diff <= -1:
                return "aggressive"
            else:
                return mode

        ### HEURISTIQUE
        # Marche qu'avec les noirs ici attention
        def advanced_heuristic(game_state, previous_state, pre_previous_state,piece_type=self.piece_type, game_type=mode_board):
            opponent_piece_type = 'W' if piece_type == 'B' else 'B'
            max_distance_score = 5

            try :
                previous_board = pre_previous_state.rep.get_grid()
                previous_piece_positions = []  # Position de nos pièces
                previousopponent_position = []

                for i in range(len(previous_board)):
                    for j in range(len(previous_board[i])):
                        if previous_board[i][j] == piece_type:  # Compter les pièces du joueur
                            previous_piece_positions.append((i, j))
                        elif previous_board[i][j] == opponent_piece_type:
                            previousopponent_position.append((i, j))
            except Exception as e:
                previous_board = "Error"

            center = (4, 4)

            board = game_state.rep.get_grid()
            score = 0
            piece_positions = []  # Position de nos pièces
            opponent_position = []
            my_piece_count = 0
            opponent_piece_count = 0

            for i in range(len(board)):
                for j in range(len(board[i])):
                    if board[i][j] == piece_type:  # Compter les pièces du joueur
                        piece_positions.append((i, j))
                        my_piece_count+=1
                    elif board[i][j] == opponent_piece_type:
                        opponent_position.append((i, j))
                        opponent_piece_count += 1


            # Déterminez le mode
            mode = determine_mode(my_piece_count, opponent_piece_count, current_state)

            if game_type == "Classique":
                # Poids des paramètres par défaut
                if mode == "neutral":
                    piece_weight = 15
                    opponent_piece_weight = 15
                    center_weight = 2
                    cohesion_weight = 30
                    push_weight = 10
                    close_weight = 2
                    mobility_weight = 10
                    three_pack_weight = 20
                    auBord_weight = 20

                # Poids des paramètres selon le mode
                if mode == "aggressive":
                    piece_weight = 15
                    opponent_piece_weight = 1000
                    center_weight = 1
                    cohesion_weight = 30
                    push_weight = 30
                    close_weight = 3
                    mobility_weight = 10
                    three_pack_weight = 20
                    auBord_weight = 10
                    depth = 3 #Ne marche pas -- > Changement directement dans alpha beta pruning

                elif mode == "defensive":
                    piece_weight = 30
                    opponent_piece_weight = 5
                    center_weight = 2
                    cohesion_weight = 50
                    push_weight = 5
                    close_weight = 2
                    mobility_weight = 10
                    three_pack_weight = 20
                    auBord_weight = 30
                    depth = 4 #Ne marche pas -- > Changement directement dans alpha beta pruning
            else : # Mode Alien
                if mode == "neutral":
                    piece_weight = 15
                    opponent_piece_weight = 15
                    center_weight = 2
                    cohesion_weight = 30
                    push_weight = 10
                    close_weight = 2
                    mobility_weight = 10
                    three_pack_weight = 20
                    auBord_weight = 20

                # Poids des paramètres selon le mode
                if mode == "aggressive":
                    piece_weight = 15
                    opponent_piece_weight = 1000
                    center_weight = 1
                    cohesion_weight = 30
                    push_weight = 30
                    close_weight = 3
                    mobility_weight = 10
                    three_pack_weight = 20
                    auBord_weight = 10
                    depth = 3 #Ne marche pas -- > Changement directement dans alpha beta pruning

                elif mode == "defensive":
                    piece_weight = 30
                    opponent_piece_weight = 5
                    center_weight = 2
                    cohesion_weight = 50
                    push_weight = 5
                    close_weight = 2
                    mobility_weight = 10
                    three_pack_weight = 20
                    auBord_weight = 30
                    depth = 4 #Ne marche pas -- > Changement directement dans alpha beta pruning


            # AJOUT SELON DISTANCE AU CENTRE
            for position in piece_positions:
                distance_score = calculate_distance_to_center(position, center)
                score += (max_distance_score - distance_score) * center_weight

            # AJOUT SELON NOS PIECES
            score += len(piece_positions) * piece_weight

            # AJOUT SELON LES PIECES ADVERSAIRES
            score -= opponent_piece_count * opponent_piece_weight

            # AJOUT SELON LA COHESION
            cohesion_score = calculate_cohesion(piece_positions)
            score -= cohesion_score * cohesion_weight

            # AJOUT SELON LA POUSSEE VERS L'ADVERSAIRE
            if (previous_board !='Error'):
                push_opponent_score = push_opponent(piece_positions, previousopponent_position)
                score += push_opponent_score*push_weight

            #AJOUT SELON LA PROXIMITE AVEC L'ADVERSAIRE
            close_opponent_score = closeTo_opponent(piece_positions, opponent_position)
            score += close_opponent_score * close_weight

            # Ne marche pas bien (donne la même mobilité pour tout)
            """"#AJOUT DE LA MOBILITE : + ACTIONS --> Mieux ?
            mobility_score = mobility(current_state, possible_actions)
            score+=mobility_score*mobility_weight
            print("SCORE MOOOOOOOOBILITE : ")
            print(mobility_score*mobility_weight)"""

            # Ne marche pas bien
            """"# AJOUT SELON LES PACKS DE 3
            three_pack_score = count_three_packs(board, piece_type)
            score += three_pack_score * three_pack_weight
            print("Score 3 packs")
            print(three_pack_score*three_pack_weight)"""

            #RETIRE points si pièces au bord -- > A voir selon l'avancé dans la partie !
            auBord_score = auBord(piece_positions)
            score-=auBord_score*auBord_weight

            return score

        depth = 3
        mode_board = mode_board if mode_board is not None else self.config_type
        _, best_action = alphabetaPurning(current_state, depth, -float('inf'), float('inf'), True, None, None, mode_board)

        return best_action