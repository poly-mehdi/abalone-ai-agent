# Abalone AI Agent
Welcome to the Abalone AI Agent project! This project was completed as part of a team challenge to develop an automated agent capable of playing Abalone effectively. The goal was to participate in a Challonge tournament where teams developed AI agents to compete against each other in the game of Abalone.

## Introduction
In this project, our team developed an AI agent programmed in Python 3 to play the game of Abalone. Our agent utilizes various strategies and techniques to make informed decisions during gameplay. We leveraged the Seahorse library and implemented custom algorithms to create a competitive Abalone AI.

To use the agent, you need to install Seahorse with the following command:

```bash
pip install seahorse==1.0.0
```
Then, you can run the command:

```bash
python main_abalone.py -t local random_player_abalone.py my_player.py
```
For other commands, refer to the PDF available in the repository.

## Getting Started
To install the Abalone AI Agent project, follow these steps:

1. Clone this repository to your local machine.
2. Install Seahorse using the provided command.
3. Review the provided PDF for additional commands and information.
4. Test the agent using the provided command.
